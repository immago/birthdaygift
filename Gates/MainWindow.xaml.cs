﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Animation;
using System.Media;
using System.Threading;

namespace Gates
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        System.Windows.Threading.DispatcherTimer dispatcherTimerOpenGate, dispatcherTimerFadeGlow;


        public MainWindow()
        {
            InitializeComponent();

            mainGame.Visibility = Visibility.Hidden;

            List<BitmapImage> images = new List<BitmapImage>();
            for (int i = 1; i <= 4; i++)
                images.Add(new BitmapImage(new Uri("Resources/hs_glow/hearthstone_" + i + ".png", UriKind.Relative)));

            for (int i = 4; i >= 1; i--)
                images.Add(new BitmapImage(new Uri("Resources/hs_glow/hearthstone_" + i + ".png", UriKind.Relative)));
            

            animatedButton.initWithImages(images);
            animatedButton.run(new TimeSpan(0, 0, 0, 0, 180));
        }


        public Bitmap TakeScreenShot()
        {
            Bitmap BitmapScreenShot;
            Graphics GraphicsScreenShot;
            BitmapScreenShot = new Bitmap(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            
            GraphicsScreenShot = Graphics.FromImage(BitmapScreenShot);
            GraphicsScreenShot.CopyFromScreen(System.Windows.Forms.Screen.PrimaryScreen.Bounds.X, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Y, 0, 0, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
           
            
            return BitmapScreenShot;
            //BitmapScreenShot.Save(FilePath, ImageFormat.Png); // you can choose jpg or another type of picture format 
            //BitmapScreenShot.Dispose();
        }

        private void start()
        {

            animatedButton.Visibility = Visibility.Hidden;

            Action EmptyDelegate = delegate() { };
            animatedButton.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, EmptyDelegate);
            
            initImages();

            animatedButton.Visibility = Visibility.Visible;
            animatedButton.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, EmptyDelegate);

            mainGame.Visibility = Visibility.Visible;

            //glow 2 sec
            glowAnimation(2, false, 0.0, 1.0);

            //preOpenGates();

            //open gate 2 sec
            dispatcherTimerOpenGate = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimerOpenGate.Tick += new EventHandler(openGate);
            dispatcherTimerOpenGate.Interval = new TimeSpan(0, 0, 2);
            dispatcherTimerOpenGate.Start();
            

            //fade glow
            dispatcherTimerFadeGlow = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimerFadeGlow.Tick += new EventHandler(fadeGlow);
            dispatcherTimerFadeGlow.Interval = new TimeSpan(0, 0, 5);
            dispatcherTimerFadeGlow.Start();

        }

        void initImages()
        {
            Bitmap bmp = TakeScreenShot();
            BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
            bmp.GetHbitmap(),
            IntPtr.Zero,
            System.Windows.Int32Rect.Empty,
            BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));


            imageLeft.Source = bs;
            imageRight.Source = bs;
        }

        void preOpenGates()
        {

            TranslateTransform transLeft = new TranslateTransform();
            imageLeft.RenderTransform = transLeft;
            DoubleAnimation anim1 = new DoubleAnimation(0, 5, TimeSpan.FromMilliseconds(50));
            anim1.AutoReverse = true;
            anim1.RepeatBehavior = new RepeatBehavior(50);
            transLeft.BeginAnimation(TranslateTransform.YProperty, anim1);

            TranslateTransform transRight = new TranslateTransform();
            imageRight.RenderTransform = transRight;
            DoubleAnimation anim2 = new DoubleAnimation(0, 5, TimeSpan.FromMilliseconds(50));
            anim2.AutoReverse = true;
            anim2.RepeatBehavior = new RepeatBehavior(50);
            transRight.BeginAnimation(TranslateTransform.YProperty, anim2);
        }

        void openGate(object sender, EventArgs e)
        {
            //open gates
            double sysWidth = System.Windows.SystemParameters.PrimaryScreenWidth;

            TranslateTransform transRight = new TranslateTransform();
            imageRight.RenderTransform = transRight;
            DoubleAnimation animRight = new DoubleAnimation(0, sysWidth / 2, TimeSpan.FromSeconds(2));
            transRight.BeginAnimation(TranslateTransform.XProperty, animRight);

            TranslateTransform transLeft = new TranslateTransform();
            imageLeft.RenderTransform = transLeft;
            DoubleAnimation animLeft = new DoubleAnimation(0, -sysWidth / 2 , TimeSpan.FromSeconds(2));
            transLeft.BeginAnimation(TranslateTransform.XProperty, animLeft);


            //move button
            TranslateTransform transButton = new TranslateTransform();
            animatedButton.RenderTransform = transButton;
            DoubleAnimation animButton = new DoubleAnimation(0, sysWidth / 2 + 50, TimeSpan.FromSeconds(2));
            transButton.BeginAnimation(TranslateTransform.XProperty, animButton);

            if (dispatcherTimerOpenGate != null)
                dispatcherTimerOpenGate.Stop();

            //open glow
            DoubleAnimation animGlow = new DoubleAnimation(sysWidth, TimeSpan.FromSeconds(2));
            Storyboard.SetTargetProperty(animGlow, new PropertyPath(Border.WidthProperty));
            Storyboard.SetTarget(animGlow, glowCenter);
            Storyboard s = new Storyboard();
            s.Children.Add(animGlow);
            s.Begin();

            //sound
            Sound.Play(Properties.Resources.open_door);
            Sound.Play(Properties.Resources.background, 4 * 1000, true);
           

        }

        void fadeGlow(object sender, EventArgs e)
        {
            glowAnimation(2, false, 1.0, 0.0);

            imageLeft.Visibility = Visibility.Hidden;
            imageRight.Visibility = Visibility.Hidden;

            if(dispatcherTimerFadeGlow!=null)
                dispatcherTimerFadeGlow.Stop();



            System.Windows.Threading.DispatcherTimer dispatcherTimer= new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(delegate(Object o, EventArgs a)
            {
                glowCenter.Visibility = Visibility.Hidden;
                dispatcherTimer.Stop();
            });

            dispatcherTimer.Interval = new TimeSpan(0, 0, 2);
            dispatcherTimer.Start();

            //System.Threading.Thread.Sleep(TimeSpan.FromSeconds(2));
            //glowCenter.Visibility = Visibility.Hidden;
        }

        void glowAnimation(int seconds, bool reverse, double from, double to)
        {
            glowCenter.Opacity = 0.0f;
            glowCenter.Visibility = Visibility.Visible;

            DoubleAnimation da = new DoubleAnimation();
            da.From = from;
            da.To = to;
            da.Duration = new Duration(TimeSpan.FromSeconds(seconds));
            da.AutoReverse = reverse;
            //da.RepeatBehavior = RepeatBehavior.Forever;
            //da.RepeatBehavior=new RepeatBehavior(3);
            glowCenter.BeginAnimation(OpacityProperty, da);
        }

        private void animatedButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            start();
        }

       

    }
}
