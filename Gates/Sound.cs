﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Media;

namespace Gates
{
    static class Sound
    {

        static SoundPlayer player;
        static Thread th;

        public static void Play(System.IO.UnmanagedMemoryStream sound, int startDelay = 0, bool loop = false)
        {
            
            th = new Thread(new ThreadStart(
                delegate 
                {
                    Thread.Sleep(startDelay);
                    player = new SoundPlayer(sound);
                    player.Play();
                }
                ));
            th.IsBackground = true;
            th.Start();

        }

        public static void Stop()
        {
            if(player!=null)
                player.Stop();
                
          //  th.Abort();
            
        }
    }
}
