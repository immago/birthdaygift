﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gates
{
    /// <summary>
    /// Interaction logic for AnimatedImages.xaml
    /// </summary>
    public partial class AnimatedImages : UserControl
    {
        List<BitmapImage>images;
        System.Windows.Threading.DispatcherTimer dT;
        int counter = 0;

        

        public AnimatedImages()
        {
            InitializeComponent();
            //this.Background = Brushes.Transparent;
        }

        public void initWithImages(List<BitmapImage> imagesNames)
        {
            image.Source = imagesNames.ElementAt(0);
            this.Background = null;//new ImageBrush(imagesNames.ElementAt(0).Clone()); // null;//Brushes.Transparent;
            counter = 0;
            images = imagesNames;
        }

        public void run(TimeSpan tickSpeed)
        {
            dT = new System.Windows.Threading.DispatcherTimer();
            dT.Interval = tickSpeed;//new TimeSpan(0, 0, 0, 0, 35);
            dT.Tick += new EventHandler(animateTick);
            dT.Start();
        }

        public void stop()
        {
            if(dT!=null)
                dT.Stop();
        }

        void animateTick(object sender, EventArgs e)
        {
            image.Source = images[counter % images.Count];
            counter++;
        }
    }
}
