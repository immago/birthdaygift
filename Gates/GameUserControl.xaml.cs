﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace Gates
{
    /// <summary>
    /// Interaction logic for GameUserControl.xaml
    /// </summary>
    public partial class GameUserControl : UserControl
    {

        int step = 0;
        
        private bool m_IsPressed = false;
        Point mouseSourcePos;
        TranslateTransform currTransform;

       

        public GameUserControl()
        {
            InitializeComponent();


            //background
           List<BitmapImage> images = new List<BitmapImage>();
           for (int i = 1; i <= 8; i++)
           {
               images.Add(new BitmapImage(new Uri("Resources/background/background_" + i + ".png", UriKind.Relative)));
           }
           backgroundAnimatedImage.initWithImages(images);
           backgroundAnimatedImage.run(new TimeSpan(0, 0, 0, 0, 80));
            
            //gomez
           List<BitmapImage> gomez = new List<BitmapImage>();
           for (int i = 1; i <= 3; i++)
               gomez.Add(new BitmapImage(new Uri("Resources/gomez_sleep/gomez_sleep_" + i + ".png", UriKind.Relative)));

           gomezRun.initWithImages(gomez);
           gomezRun.run(new TimeSpan(0, 0, 0, 0, 380));

           setTalkText("", Visibility.Hidden);
           buttonVariant1.Content = "[Разбудить]";

           }

        void setTalkText(string text, Visibility visible)
        {
            textBlockTalk.Text = text;
            textBlockTalk.Visibility = visible;
        }

        //bitton1
        private void buttonVariant1_Click(object sender, RoutedEventArgs e)
        {
            switch(step)
            {
                case 0:
                    {
                        List<BitmapImage> gomez = new List<BitmapImage>();
                        gomez.Add(new BitmapImage(new Uri("Resources/gomez_normal.png", UriKind.Relative)));
                        gomezRun.initWithImages(gomez);

                        setTalkText("А что? Сколько я проспал? ЧТО! Уже " + DateTime.Now.ToString("dd/MM/yyyy") + "?! Я же не подготовил подарок. Подожди минутку.",
                                    Visibility.Visible);
                        buttonVariant1.Content = "Ладно, ладно, успокойся.";
                        step++;
                        break;
                    }
                case 1:
                    {
                        setTalkText("\nОЙ, кнопка. Мне не пройти. Убери ее пожалуйста с прохода.", Visibility.Visible);
                        buttonVariant1.Content = "";
                        step++;
                        break;
                    }
                case 3:
                    {
                        setTalkText("", Visibility.Hidden);

                        //animate
                        List<BitmapImage> gomez = new List<BitmapImage>();
                        for (int i = 1; i <= 6; i++)
                            gomez.Add(new BitmapImage(new Uri("Resources/gomez_run/gomez_run_" + i + ".png", UriKind.Relative)));

                        gomezRun.initWithImages(gomez);
                        gomezRun.run(new TimeSpan(0, 0, 0, 0, 80));

                        //move
                        TranslateTransform trans = new TranslateTransform();
                        gomezRun.RenderTransform = trans;
                        DoubleAnimation anim1 = new DoubleAnimation(0,
                                                                    System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width / 2 - gomezRun.Width / 2,
                                                                    TimeSpan.FromSeconds(4));
                        trans.BeginAnimation(TranslateTransform.XProperty, anim1);

                        //stop after go
                        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                        dispatcherTimer.Tick += new EventHandler(delegate(Object o, EventArgs a)
                        {
                            gomez = new List<BitmapImage>();
                            gomez.Add(new BitmapImage(new Uri("Resources/gomez_happy.png", UriKind.Relative)));
                            gomezRun.initWithImages(gomez);
                            dispatcherTimer.Stop();
                        });
                        dispatcherTimer.Interval = new TimeSpan(0, 0, 4);
                        dispatcherTimer.Start();

                        //button opacity
                        buttonVariant1.IsEnabled = false;
                        DoubleAnimation da = new DoubleAnimation();
                        da.From = 1d;
                        da.To = 0d;
                        da.Duration = new Duration(TimeSpan.FromSeconds(3));
                        buttonVariant1.BeginAnimation(OpacityProperty, da);

                        //background fade
                        DoubleAnimation backgrFade = new DoubleAnimation();
                        backgrFade.From = 1d;
                        backgrFade.To = 0d;
                        backgrFade.Duration = new Duration(TimeSpan.FromSeconds(4 + 6));
                        backgroundAnimatedImage.BeginAnimation(OpacityProperty, backgrFade);

                        //light
                        System.Windows.Threading.DispatcherTimer dispatcherTimerLight = new System.Windows.Threading.DispatcherTimer();
                        dispatcherTimerLight.Tick += new EventHandler(delegate(Object o, EventArgs a)
                        {
                            light.Visibility = Visibility.Visible;
                            DoubleAnimation animLight = new DoubleAnimation(50, TimeSpan.FromSeconds(2));
                            Storyboard.SetTargetProperty(animLight, new PropertyPath(Border.WidthProperty));
                            Storyboard.SetTarget(animLight, light);
                            Storyboard s = new Storyboard();
                            s.Children.Add(animLight);
                            s.Begin();

                            dispatcherTimerLight.Stop();
                        });
                        dispatcherTimerLight.Interval = new TimeSpan(0, 0, 4);
                        dispatcherTimerLight.Start();

                        //gift
                        System.Windows.Threading.DispatcherTimer dispatcherTimerGift = new System.Windows.Threading.DispatcherTimer();
                        dispatcherTimerGift.Tick += new EventHandler(delegate(Object o, EventArgs a)
                        {
                            TranslateTransform transformGift = new TranslateTransform();
                            gift.RenderTransform = transformGift;
                            DoubleAnimation animGift = new DoubleAnimation(0, System.Windows.SystemParameters.PrimaryScreenHeight - (45+90), TimeSpan.FromSeconds(6));
                            animGift.Completed += letsRock;
                            transformGift.BeginAnimation(TranslateTransform.YProperty, animGift);
                            
                            dispatcherTimerGift.Stop();
                        });
                        dispatcherTimerGift.Interval = new TimeSpan(0, 0, 4);
                        dispatcherTimerGift.Start();

  
                        break;
                    }
            }

        }


        void letsRock(Object o, EventArgs a)
        {
            gomezRun.Visibility = Visibility.Hidden;
            light.Visibility = Visibility.Hidden;
            gift.Visibility = Visibility.Hidden;

            gomezRock.Visibility = Visibility.Visible;

            List<BitmapImage> gomez = new List<BitmapImage>();
            for (int i = 1; i <= 13; i++)
                gomez.Add(new BitmapImage(new Uri("Resources/fez_rock/fez_rock_" + i + ".png", UriKind.Relative)));

            gomezRock.initWithImages(gomez);
            gomezRock.run(new TimeSpan(0, 0, 0, 0, 60));


            //text
            labelBirthday.Visibility = Visibility.Visible;
            System.Windows.Threading.DispatcherTimer dispatcherTimerText = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimerText.Tick += new EventHandler(delegate(Object o1, EventArgs a1)
            {
                labelBirthday.Foreground = PickBrush();
            });
            dispatcherTimerText.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimerText.Start();

            //music
            Sound.Stop();
            Sound.Play(Properties.Resources.happy_birthday, 0, true);

        }

        private Brush PickBrush()
        {
            Brush result = Brushes.Transparent;

            Random rnd = new Random();

            Type brushesType = typeof(Brushes);

            System.Reflection.PropertyInfo[] properties = brushesType.GetProperties();

            int random = rnd.Next(properties.Length);
            result = (Brush)properties[random].GetValue(null, null);

            return result;
        }

        private void buttonVariant1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                m_IsPressed = true;
                mouseSourcePos = Mouse.GetPosition(LayoutRoot);
                currTransform = buttonVariant1.RenderTransform as TranslateTransform;
            }
            else
            {
                m_IsPressed = false;
            }
        }

        private void buttonVariant1_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            m_IsPressed = false;

            TranslateTransform transform = buttonVariant1.RenderTransform as TranslateTransform;
            if (transform != null)
            {
                if ((transform.Y < -100) && (step == 2))
                {
                    step = 3;
                }
            }
        }

        private void buttonVariant1_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if ((m_IsPressed)&&(step==2))
            {


                Point relativePoint1 = buttonVariant1.TransformToAncestor(LayoutRoot).Transform(new Point(0, 0));

                TranslateTransform transform = new TranslateTransform();

                double cx = 0, cy = 0;
                if (currTransform != null)
                {
                    cx = currTransform.X;
                    cy = currTransform.Y;
                }

                transform.X = Mouse.GetPosition(LayoutRoot).X - mouseSourcePos.X + cx;
                transform.Y = Mouse.GetPosition(LayoutRoot).Y - mouseSourcePos.Y + cy;
                this.buttonVariant1.RenderTransform = transform;
            }
        }


    }
}
